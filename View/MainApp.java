/*
 * Delwyn Monaghan & John McDonnell.
 * OOD - CA2.
 * Flight Control System.
 * Version Final.
 * Due Date: 14/12/2014.
 */

package View;

/*
 * Imports.
 */
import java.util.Scanner;
import Control.InputValidationUtility;
import Control.Menu;
import Control.PlaneStore;
import Model.Plane;

public class MainApp
{
	/*
	 * Declaring mainMenu & kb scanner.
	 */
	private Menu mainMenu;
	private Scanner kb;

	/*
	 * Creating a MainApp object. Start method.
	 */
	public static void main(String[] args)
	{
		MainApp theApp = new MainApp();
		theApp.start();
	}

	/*
	 * Passing Map into PLaneStore. Initializing menu system . Initializing
	 * prompts within menu.
	 */
	private void start()
	{
		PlaneStore store = initaliseMap();
		initialiseMenus();
		menu(store);
	}

	/*
	 * Menu method. Gives access to plane information. Information can be
	 * entered. Loops until user wants to exit.
	 */
	private void menu(PlaneStore store)
	{

		this.kb = new Scanner(System.in);

		/*
		 * Declaring mainChoice (users input).
		 */
		int mainChoice = 0;

		/*
		 * Loops until 10 is entered.
		 */
		while (mainChoice != 11)
		{
			/*
			 * Retrieves method from menu class. Returns information to
			 * mainChoice.
			 */
			mainChoice = this.mainMenu.printAndGetMainChoice("Enter choice: ");

			/*
			 * Menu option 1 - add a plane
			 */
			if (mainChoice == 1)
			{
				/*
				 * prompts users for plane information.
				 */

				System.out.println("\nPlease add new plane details");
				String strPrompt = "\nPlease enter airline you would like to add to";

				String airline = InputValidationUtility.getString(this.kb,
						strPrompt);
				strPrompt = "\nFlight number";

				String fn = InputValidationUtility
						.getString(this.kb, strPrompt);
				strPrompt = "\nFuel remaining (in litres)";

				Double fr = InputValidationUtility
						.getDouble(this.kb, strPrompt);
				strPrompt = "\nAirplane type";

				String at = InputValidationUtility
						.getString(this.kb, strPrompt);
				strPrompt = "\nTime overdue (in minutes)";

				int to = InputValidationUtility.getInt(this.kb, strPrompt);
				strPrompt = "\nAmount of passengers";

				int p = InputValidationUtility.getInt(this.kb, strPrompt);

				/*
				 * adds the above information and create a new plane object in
				 * PlaneStore.
				 */
				store.add(airline, new Plane(fn, airline, fr, at, to, p, 0));

			}
			/*
			 * Menu option 2 - remove a plane. Flight number is used to remove a
			 * plane. Flight number is unique.
			 */
			else if (mainChoice == 2)
			{
				System.out.println("\nYou have chosen to delete a plane");
				System.out
						.println("\nPlease enter flight number of the plane you would like to delete");
				String flightNumber = kb.next();

				/*
				 * Searches for flight number, removes it from the plane set.
				 */
				store.removeByFlightNumber(flightNumber);

			}
			/*
			 * Menu option 3 - next plane to land.
			 */
			else if (mainChoice == 3)
			{
				System.out
						.println("\nYou have chosen to print the next plane to land for a specified airline");
				System.out
						.println("\nPlease enter airline name to print the next plane to land for that airline");
				String airline = kb.next();

				/*
				 * Searches each airline. Calculates fuel remaining. Prints the
				 * plane which needs to land as fuel is low.
				 */
				store.printNextToLand(airline);
			}
			/*
			 * Menu option 4 - last plane to land.
			 */
			else if (mainChoice == 4)
			{
				System.out
						.println("\nYou have chosen to print the Last plane to land for a specified airline");
				System.out
						.println("\nPlease enter airline name to print the last plane to land for that airline");
				String airline = kb.next();
				/*
				 * Searches each airline. Calculates fuel remaining. Prints the
				 * plane which has more fuel than others and can land last.
				 */
				store.printLastToLand(airline);
			}
			/*
			 * Menu option 5 - Airline with most planes in the air.
			 */
			else if (mainChoice == 5)
			{
				System.out
						.println("\nYou have chosen to print the name of the airline with the most planes in the air");

				/*
				 * Counts each plane a airline has in the air.
				 */
				store.printByLargestAirline();
			}
			/*
			 * Menu option 6 - Print total number of passengers.
			 */
			else if (mainChoice == 6)
			{
				System.out
						.println("\nYou have chosen to print the total number of passengers in the air");
				/*
				 * Counts the total number is passengers in the air.
				 */
				store.printTotalOfPassengers();
			}
			/*
			 * Menu option 7 - Print by airline based on time overdue
			 */
			else if (mainChoice == 7)
			{
				System.out
						.println("\nYou have chosen to print the details of the airplanes scheduled to land for a specified airline sorted by the amount of time the airplanes are overdue on arrival");
				/*
				 * Searching through each airline. Sorting based on highest time
				 * overdue.
				 */
				store.copy();
			}
			/*
			 * Menu option 8 - Save and Load a file.
			 */
			else if (mainChoice == 8)
			{
				System.out
						.println("\nYou have chosen to save and load to a file");
				/*
				 * Uses SerializationUtility to save and load a file.
				 */
				store.saveAndLoad();
			}
			/*
			 * Menu option 9 - Print all planes in the map.
			 */
			else if (mainChoice == 9)
			{
				System.out
						.println("\nYou have chosen to print out all airlines");
				/*
				 * Print all planes
				 */
				store.printMap();
			}
			/*
			 * Menu option 10 - Exit the application.
			 */
			else if (mainChoice == 10)
			{
				System.out.println("\nSystem is now shutting down");
				System.exit(0);
			}
			/*
			 * If input is invalid message is prompted.
			 */
			else
			{
				System.out.println("Input not valid please try again");
			}

		}
	}

	/*
	 * Creating a menu object. Stores Strings.
	 */
	public void initialiseMenus()
	{
		System.out
				.println("\n------------------------------ Flight Control System ------------------------------\n");
		this.mainMenu = new Menu("Flight Control System Menu");

		/*
		 * Adding to the menu object.
		 */
		this.mainMenu.add("Add the airplane details of a new plane.");
		this.mainMenu
				.add("Delete the airplane details of any airplane that has landed.");
		this.mainMenu
				.add("Print the details of the next airplane scheduled to land for a specified airline.");
		this.mainMenu
				.add("Print the details of the last airplane scheduled to land for a specified airline.");
		this.mainMenu
				.add("Print the name of the airline with the most airplanes in the air.");
		this.mainMenu.add("Print the Total Number Of Passengers In The Air.");
		this.mainMenu
				.add("Print the details of the airplanes scheduled to land for a specified airline");
		this.mainMenu
				.add("Load from and store to a text file the contents of the Flight Control System.");
		this.mainMenu.add("Print Out All Airlines");
		this.mainMenu.add("Exit Application \n");

	}

	private PlaneStore initaliseMap()
	{

		/*
		 * Creating a new PlaneStore object.
		 */
		PlaneStore store = new PlaneStore();

		/*
		 * HashMap which holds a String and a TreeSet of type Plane. Adding new
		 * planes to each airline in the following order. Airline Name - Flight
		 * Number - Airline - Fuel Remaining - Type - Time overdue - Passenger
		 * Number - timeLimitBeforeLand.
		 */

		/*
		 * Ryanair planes.
		 */
		store.add("Ryanair", new Plane("FR107", "Ryanair", 5921.00,
				"Airbus A380", -2, 112, 0));
		store.add("Ryanair", new Plane("FR108", "Ryanair", 6238.00,
				"Airbus A300", -4, 145, 0));
		store.add("Ryanair", new Plane("FR109", "Ryanair", 6687.00,
				"Airbus A380", 2, 257, 0));
		store.add("Ryanair", new Plane("FR110", "Ryanair", 7304.00,
				"Boeing 300F", 0, 118, 0));
		store.add("Ryanair", new Plane("FR101", "Ryanair", 2510.00,
				"Boeing KC-46", 0, 120, 0));
		store.add("Ryanair", new Plane("FR102", "Ryanair", 4800.00,
				"Boeing 747", 67, 125, 0));
		store.add("Ryanair", new Plane("FR104", "Ryanair", 5452.00,
				"Boeing 787", -3, 135, 0));
		store.add("Ryanair", new Plane("FR103", "Ryanair", 5215.00,
				"Boeing 767", 103, 130, 0));
		store.add("Ryanair", new Plane("FR105", "Ryanair", 5540.00,
				"Boeing 737", 0, 140, 0));
		store.add("Ryanair", new Plane("FR106", "Ryanair", 5896.00,
				"Boeing KC-767", 90, 98, 0));

		/*
		 * Air Canada planes.
		 */
		store.add("Air Canada", new Plane("AC211", "Air Canada", 2058.00,
				"Airbus A380", 0, 150, 0));
		store.add("Air Canada", new Plane("AC212", "Air Canada", 2331.00,
				"Airbus A380", 20, 175, 0));
		store.add("Air Canada", new Plane("AC213", "Air Canada", 2361.00,
				"Airbus A330", 341, 300, 0));
		store.add("Air Canada", new Plane("AC214", "Air Canada", 2841.00,
				"Airbus A319", -20, 320, 0));
		store.add("Air Canada", new Plane("AC215", "Air Canada", 3549.00,
				"Boeing 300F", 0, 186, 0));
		store.add("Air Canada", new Plane("AC216", "Air Canada", 4562.00,
				"Boeing 737", 90, 97, 0));
		store.add("Air Canada", new Plane("AC217", "Air Canada", 4756.00,
				"Boeing 747", -2, 114, 0));
		store.add("Air Canada", new Plane("AC218", "Air Canada", 5810.00,
				"Boeing KC-767", -4, 333, 0));

		/*
		 * Air France planes.
		 */
		store.add("Air France", new Plane("AF319", "Air France", 5937.00,
				"Boeing 787", 77, 254, 0));
		store.add("Air France", new Plane("AF320", "Air France", 6664.00,
				"Boeing 737", 0, 101, 0));

		/*
		 * Eithad planes.
		 */
		store.add("Etihad", new Plane("EY421", "Etihad", 6766.00,
				"Boeing 300F", -5, 119, 0));
		store.add("Etihad", new Plane("EY422", "Etihad", 6911.00,
				"Airbus A380", 10, 107, 0));
		store.add("Etihad", new Plane("EY423", "Etihad", 7154.00,
				"Airbus A380", 8, 311, 0));
		store.add("Etihad", new Plane("EY424", "Etihad", 8608.00,
				"Airbus A390", -7, 339, 0));
		store.add("Etihad", new Plane("EY425", "Etihad", 8869.00,
				"Airbus A330", 1, 69, 0));
		store.add("Etihad", new Plane("EY426", "Etihad", 2312.00,
				"Airbus A330", -6, 114, 0));
		store.add("Etihad", new Plane("EY427", "Etihad", 2552.00,
				"Airbus A300", -4, 325, 0));
		store.add("Etihad", new Plane("EY428", "Etihad", 3529.00,
				"Airbus A319", 12, 350, 0));
		store.add("Etihad", new Plane("EY429", "Etihad", 3974.00, "Boeing 737",
				17, 322, 0));

		/*
		 * Aerlingus planes.
		 */
		store.add("Aerlingus", new Plane("EI530", "Aerlingus", 4512.00,
				"Boeing 747", 25, 210, 0));
		store.add("Aerlingus", new Plane("EI531", "Aerlingus", 5825.00,
				"Boeing 767", -16, 75, 0));
		store.add("Aerlingus", new Plane("EI532", "Aerlingus", 6230.00,
				"Boeing 787", 12, 213, 0));
		store.add("Aerlingus", new Plane("EI533", "Aerlingus", 6409.00,
				"Boeing 737", -10, 320, 0));
		store.add("Aerlingus", new Plane("EI534", "Aerlingus", 6524.00,
				"Boeing 300F", 0, 180, 0));
		store.add("Aerlingus", new Plane("EI535", "Aerlingus", 7288.00,
				"Boeing 300F", 0, 188, 0));

		/*
		 * British Airways planes.
		 */
		store.add("British Airways", new Plane("BA636", "British Airways",
				7565.00, "Boeing 767", 55, 302, 0));
		store.add("British Airways", new Plane("BA637", "British Airways",
				8282.00, "Boeing 787", -8, 320, 0));
		store.add("British Airways", new Plane("BA638", "British Airways",
				8807.00, "Boeing 300F", 0, 186, 0));
		store.add("British Airways", new Plane("BA639", "British Airways",
				9591.00, "Boeing 787", 0, 97, 0));
		store.add("British Airways", new Plane("BA640", "British Airways",
				9624.00, "Airbus A319", 0, 114, 0));
		store.add("British Airways", new Plane("BA641", "British Airways",
				2427.00, "Airbus A390", -4, 203, 0));
		store.add("British Airways", new Plane("BA642", "British Airways",
				2655.00, "Airbus A390", 23, 297, 0));
		store.add("British Airways", new Plane("BA643", "British Airways",
				3401.00, "Boeing 747", 121, 168, 0));

		/*
		 * Quantas planes.
		 */
		store.add("Qantas", new Plane("QU744", "Qantas", 3516.00,
				"Airbus A390", 201, 140, 0));
		store.add("Qantas", new Plane("QU745", "Qantas", 4180.00,
				"Airbus A330", -35, 185, 0));
		store.add("Qantas", new Plane("QU746", "Qantas", 5912.00,
				"Airbus A319", 27, 303, 0));
		store.add("Qantas", new Plane("QU747", "Qantas", 6494.00,
				"Airbus A300", -11, 154, 0));
		store.add("Qantas", new Plane("QU748", "Qantas", 7244.00,
				"Airbus A380", -5, 252, 0));
		store.add("Qantas", new Plane("QU749", "Qantas", 7997.00,
				"Boeing 300F", 2, 257, 0));
		store.add("Qantas", new Plane("QU750", "Qantas", 8257.00,
				"Boeing KC-767", 0, 108, 0));


		/*
		 * Performance tests.
		 */
		
		/*
		long xtfinal = 0;
		int count = 1000;
		for (int i = 0; i < 1000; i++)
		{
			long add = tAdd(store);
			long remove = tRemove(store);
			long next = tNext(store);
			long print = tPrint(store);

			long tFinal = (add + remove + next + print);
			xtfinal = tFinal;

			System.out.println("Execution for tAdd: " + add + "microsecs");
			System.out
					.println("Execution for tRemove: " + remove + "microsecs");
			System.out.println("Execution for tNext: " + next + "microsecs");
			System.out.println("Execution for tPrint: " + print + "microsecs");

			System.out.println("\nAverage for tFinal: " + tFinal + "microsecs");

			long x = xtfinal / 1000;
			System.out.println(x);
		}
		*/

		/*
		 * Returning these values to the store.
		 */
		return store;
	}

	/*
	 * Performance test methods. tAdd. tRemove. tNext. tPrint. tFinal.
	 */
	private long tAdd(PlaneStore store)
	{
		/*
		 * Start & Stop
		 */
		long start = System.nanoTime();
		store.add("ryanair", new Plane("FR999", "RyanAir", 5921.00,
				"Airbus A380", -2, 112, 0));
		long duration = System.nanoTime() - start;

		/*
		 * Return calculation
		 */
		return duration /= 1000; // convert sum from ns to microsecs
	}

	private long tNext(PlaneStore store)
	{
		long start = System.nanoTime();
		store.printNextToLand("ryanair");
		long duration = System.nanoTime() - start;

		return duration /= 1000; // convert sum from ns to microsecs
	}

	private long tRemove(PlaneStore store)
	{
		long start = System.nanoTime();
		store.removeByFlightNumber("FR999");
		long duration = System.nanoTime() - start;

		return duration /= 1000; // convert sum from ns to microsecs
	}

	private long tPrint(PlaneStore store)
	{
		long start = System.nanoTime();
		store.printMap();
		long duration = System.nanoTime() - start;

		return duration /= 1000; // convert sum from ns to microsecs
	}
}
