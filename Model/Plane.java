/*
 * Delwyn Monaghan & John McDonnell.
 * OOD - CA2.
 * Flight Control System.
 * Version Final.
 * Due Date: 14/12/2014.
 */
package Model;

import java.io.Serializable;

public class Plane implements Serializable, Comparable<Plane>
{
	/*
	 * Instance variable
	 */
	private String flightNumber;
	private String airlineName;
	private double fuelRemaining;
	private String airplaneType;
	private int timeOverdue;
	private int passengerNumber;
	private double timeLimitBeforeLand;

	/*
	 * Constructor.
	 */
	public Plane(String flightNumber, String airlineName, double fuelRemaining,
			String airplaneType, int timeOverdue, int passengerNumber,
			double timeLimitBeforeLand)
	{
		this.flightNumber = flightNumber;
		this.airlineName = airlineName;
		this.fuelRemaining = fuelRemaining;
		this.airplaneType = airplaneType;
		this.timeOverdue = timeOverdue;
		this.passengerNumber = passengerNumber;
		this.timeLimitBeforeLand = timeLimitBeforeLand;
	}

	/*
	 * Getters and Setters.
	 */
	public String getFlightNumber()
	{
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber)
	{
		this.flightNumber = flightNumber;
	}

	public String getAirlineName()
	{
		return airlineName;
	}

	public void setAirlineName(String airlineName)
	{
		this.airlineName = airlineName;
	}

	public double getFuelRemaining()
	{
		return fuelRemaining;
	}

	public void setFuelRemaining(double fuelRemaining)
	{
		this.fuelRemaining = fuelRemaining;
	}

	public String getAirplaneType()
	{
		return airplaneType;
	}

	public void setAirplaneType(String airplaneType)
	{
		this.airplaneType = airplaneType;
	}

	public int getTimeOverdue()
	{
		return timeOverdue;
	}

	public void setTimeOverdue(int timeOverdue)
	{
		this.timeOverdue = timeOverdue;
	}

	public int getPassengerNumber()
	{
		return passengerNumber;
	}

	public void setPassengerNumber(int passengerNumber)
	{
		this.passengerNumber = passengerNumber;
	}

	public double getTimeLimitBeforeLand()
	{
		return timeLimitBeforeLand;
	}

	public void setTimeLimitBeforeLand(double timeLimitBeforeLand)
	{
		this.timeLimitBeforeLand = timeLimitBeforeLand;
	}

	/*
	 * To string.
	 */
	@Override
	public String toString()
	{
		return "\nPlane [flightNumber=" + flightNumber + ", airlineName="
				+ airlineName + ", fuelRemaining=" + fuelRemaining
				+ ", airplaneType=" + airplaneType + ", timeOverdue="
				+ timeOverdue + ", passengerNumber=" + passengerNumber
				+ ", timeLimitBeforeLand=" + timeLimitBeforeLand + "]\n";
	}
	/*
	 * CompareTo method.
	 * Compares time before land.
	 */
	@Override
	public int compareTo(Plane arg0)
	{
		if (arg0 instanceof Plane)
		{
			if (this.getTimeLimitBeforeLand() > arg0.getTimeLimitBeforeLand())
			{
				return 1;
			}
			else if (this.getTimeLimitBeforeLand() < arg0
					.getTimeLimitBeforeLand())
			{
				return -1;
			}
		}
		return 0;
	}

}
