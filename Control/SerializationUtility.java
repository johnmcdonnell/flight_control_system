/*
 * John McDonnell.
 * OOD - CA2.
 * Flight Control System.
 * Version Final.
 * Due Date: 14/12/2014.
 */
package Control;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

//public class SerializationUtility 
//{
/*	public static void save(String path, String name, 
									Object obj)
	{
		try
		{
			//handle to a file output stream
			FileOutputStream fos = new FileOutputStream(path + name);	
			//handle to an object output stream
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			//write the object to the stream
			oos.writeObject(obj);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static Object load(String path, String name)
	{
		Object obj = null;
		
		try
		{
			FileInputStream fis = new FileInputStream(path + name);
			ObjectInputStream ois = new ObjectInputStream(fis);
			obj = ois.readObject();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return obj;
	}
	
	public static boolean exists(String path, String name)
	{
		File file = new File(path + name);
		return file.exists();
	}
	
	*/
	
	
	import java.io.FileInputStream;
	import java.io.FileOutputStream;
	import java.io.ObjectInputStream;
	import java.io.ObjectOutputStream;

	/*
	 * Utility methods to store ANY type of storage container
	 * e.g. LinkedList<TreeMap<A, B>>
	 * 				or
	 * 		TreeMap<A, HashMap<B, C>>
	 * */
	public class SerializationUtility
	{
		//use with ANY storage type i.e. List, Map, or Set
		@SuppressWarnings("unused")
		public static void store(String fileName, Object object)
		{	
			try
			{
				//open file to write
				FileOutputStream fileOut = new FileOutputStream(fileName);
				//open stream to file
			    ObjectOutputStream out = new ObjectOutputStream(fileOut);			    
			    //write contents
				out.writeObject(object);
				//close file streams
				fileOut.close();
				out.close();
			}
			catch(Exception e)
			{
			}
		}
			
		//use with ANY storage type i.e. List, Map, or Set
		@SuppressWarnings("unused")
		public static Object load(String fileName)
		{	
			try
			{
				//open file to read
				FileInputStream fileIn = new FileInputStream(fileName);
				//open stream to file
				ObjectInputStream in = new ObjectInputStream(fileIn);
				//read contents and cast as specific data type
				Object map = in.readObject();
				//close file streams
				in.close();
				fileIn.close();
				//return loaded data
				return map;	
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			//fail
			return null;
		}
	}




















