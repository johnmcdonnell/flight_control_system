/*
 * John McDonnell.
 * OOD - CA2.
 * Flight Control System.
 * Version Final.
 * Due Date: 14/12/2014.
 */

package Control;

/*
 * Imports.
 */
import java.util.Comparator;
import Model.Plane;

/*
 * Comparator sorts plane objects.
 */

public class PlaneTimeLimitBeforeLandComparator implements Comparator<Plane>
{
	/*
	 * Declaring sort order.
	 */
	private int sortOrder;

	/*
	 * Sorting object order.
	 */
	public PlaneTimeLimitBeforeLandComparator(int sortOrder)
	{
		this.sortOrder = sortOrder; // 1 or -1
	}

	/*
	 * Comparing time limit before land. Sorting and returning new order.
	 */
	public int compare(Plane p0, Plane p1)
	{
		double difference = p0.getTimeLimitBeforeLand()
				- p1.getTimeLimitBeforeLand();

		if (difference > 0)
		{

			return 1 * sortOrder;
		}
		else if (difference < 0)
		{
			return -1 * sortOrder;
		}
		else
		{
			return 0;
		}
	}

}
