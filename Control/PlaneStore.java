/*
 * John McDonnell.
 * OOD - CA2.
 * Flight Control System.
 * Version Final.
 * Due Date: 14/12/2014.
 */

/*
 * TreeSet used throughout as it continuously sorts 
 */
package Control;

/*
 * Imports.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Collections;
import java.util.Scanner;

import Model.Plane;

/*
 * Plane Class.
 * Holds methods and calculations for application.
 */
public class PlaneStore
{
	/*
	 * Declaring TreeSet of plane. Declaring HashMap of String, TreeSet of type
	 * Plane called map. Declaring HashMap String, Double for fuelConsumption.
	 * Declaring kb Scanner.
	 */
	private TreeSet<Plane> set;
	private HashMap<String, TreeSet<Plane>> map;
	private HashMap<String, Double> fuelConsumption;
	private Scanner kb;

	/*
	 * Constructor.
	 */
	public PlaneStore()
	{
		this.set = new TreeSet<Plane>();
		this.map = new HashMap<String, TreeSet<Plane>>();
		this.fuelConsumption = new HashMap<String, Double>();
		/*
		 * Calling fuelMap.
		 */
		initaliseFuelMap();

	}
	/*
	 * Contains all plane type fuel consumption per minute.
	 */
	private void initaliseFuelMap()
	{
		this.fuelConsumption.put("Boeing_A380", 110.00);
		this.fuelConsumption.put("Airbus_800", 310.00);
		this.fuelConsumption.put("Boeing_737", 450.00);
		this.fuelConsumption.put("Airbus_320", 500.00);
		this.fuelConsumption.put("Boeing_707", 230.00);
		this.fuelConsumption.put("Airbus_340", 550.00);
		this.fuelConsumption.put("Boeing KC-46", 317.00);
		this.fuelConsumption.put("Boeing 747", 331.00);
		this.fuelConsumption.put("Boeing 767", 345.00);
		this.fuelConsumption.put("Boeing 787", 359.00);
		this.fuelConsumption.put("Boeing 737", 377.00);
		this.fuelConsumption.put("Boeing KC-767", 430.00);
		this.fuelConsumption.put("Boeing 300F", 480.00);
		this.fuelConsumption.put("Airbus A380", 578.00);
		this.fuelConsumption.put("Airbus A300", 662.00);
		this.fuelConsumption.put("Airbus A319", 655.00);
		this.fuelConsumption.put("Airbus A330", 685.00);
		this.fuelConsumption.put("Airbus A390", 745.00);

	}

	/*
	 * Add method
	 * 
	 */
	public void add(String airline, Plane p)
	{
		this.kb = new Scanner(System.in);
		/*
		 * Checks for new plane type and asks for its fuel consumption.
		 * Stores user input as double.
		 */
		if (this.getFuelConsumption(p.getAirplaneType()) < 1)
		{
			System.out.println("\nPlease enter fuel consumption for " + p.getAirplaneType());
			double fc = kb.nextDouble();
			/*
			 * Puts new values into fuel consumption.
			 */
			this.fuelConsumption.put(p.getAirplaneType(), fc);
		}
		
		/*
		 * Checks if airline exists.
		 * Add it to the TreeSet.
		 * Get its timeLimitBeforeLand, type and fuel remaining.
		 */
		if (map.containsKey(airline))
		{ 
			TreeSet<Plane> set = map.get(airline);
			p.setTimeLimitBeforeLand(this.getTimeLimitBeforeLand(
					p.getAirplaneType(), p.getFuelRemaining()));

			/*
			 * Add it to the TreeSet.
			 */
			set.add(p);

			/*
			 * Add new plane to the matching airline.
			 */
			map.put(airline, set);
		}
		else
		{
			/*
			 * If the airline didn't exist.
			 * Get its information.
			 * Add it to the TreeSet.
			 */
			TreeSet<Plane> set = new TreeSet<Plane>();
			p.setTimeLimitBeforeLand(this.getTimeLimitBeforeLand(
					p.getAirplaneType(), p.getFuelRemaining()));
			set.add(p);
			map.put(airline, set);
		}

	}

	/*
	 * Remove method - Remove by flight number.
	 */
	public void removeByFlightNumber(String flightNumber)
	{
		/*
		 * Loops through the airlines.
		 */
		for (String key : map.keySet())
		{
			/*
			 * Put map into TreeSet of planes.
			 */
			TreeSet<Plane> set = map.get(key);
			/*
			 * Loop through the planes within each airline.
			 */
			for (Plane p : set)
			{ 
				/*
				 * If flight number is found then remove it from the set.
				 */
				if (p.getFlightNumber().equalsIgnoreCase(flightNumber))
				{
					set.remove(p);
					break;
				}
			}
		}
	}

	/*
	 * Print method - Total number of passengers in the air.
	 */
	public void printTotalOfPassengers()
	{
		/*
		 * Declare count.
		 */
		int count = 0;
		/*
		 * Loop through each airline.
		 */
		for (String key : map.keySet())
		{
			/*
			 * Put map into a TreeSet of planes.
			 */
			TreeSet<Plane> set = map.get(key);
			/*
			 * Loop through the planes within each airline.
			 * Count the number of passengers on board.
			 * Print the total.
			 */
			for (Plane p : set)
			{
				count = count + p.getPassengerNumber();
			}
		}
		System.out.println("\nNumber of passengers in the air is " + count);
	}

	/*
	 * Print method - Name of airline with most planes in the air
	 */
	public void printByLargestAirline()
	{
		/*
		 * Declare variables.
		 */
		int count = 0;
		int total = 0;
		String airlineName = "";
		
		/*
		 * Loop through each airline.
		 * Count each plane.
		 * Compare them against each other.
		 * Print the airline with the most airplanes in the air.
		 */
		for (String key : map.keySet())
		{
			count = 0;
			for (int i = 0; i < map.get(key).size(); i++)
			{
				count++;
			}
			if (count > total)
			{
				total = count;
				airlineName = key;
			}
			else if (count == total)
			{
				total = count;
				airlineName = key + " and " + airlineName;
			}
		}
		System.out.println("The airline with most airplanes in the air is " + airlineName);
	}

	/*
	 * Print method - By airline.
	 * Gets airlines and places them in TreeSet Plane.
	 * Prints set.
	 */
	public void printByAirline(String airline)
	{
		if (map.containsKey(airline))
		{
			TreeSet<Plane> set = map.get(airline);
			System.out.println(set);
		}
	}

	/*
	 * Print method - Print map
	 */
	public void printMap()
	{
		System.out.println(map);
	}

	/*
	 * Print method - Next to land by a specific airline
	 * Compares fuel remaining.
	 */
	public void printNextToLand(String airline)
	{
		/*
		 * Check airline matches
		 */
		if (map.containsKey(airline))
		{
			/*
			 * Put map into TreeSet plane.
			 * TreeSet Sorts.
			 */
			TreeSet<Plane> set = map.get(airline);
			
			/*
			 * Iterate through plane.
			 * Looks through the set.
			 * Prints plane that needs to land next.
			 */
			Iterator i = set.iterator();
			if (i.hasNext())
			{
				System.out.println("next to land for " + airline + " "
						+ i.next());
			}
		}
	}

	/*
	 * Print Method - Last to land by a specific airline
	 */
	public void printLastToLand(String airline)
	{
		/*
		 * Check airline matches
		 */
		if (map.containsKey(airline))
		{
			/*
			 * Put map into TreeSet plane.
			 * TreeSet sorts,
			 */
			TreeSet<Plane> set = map.get(airline);
			
			/*
			 * Iterate through plane.
			 * Looks through the set.
			 * Prints plane that can land last.
			 */
			Iterator i = set.descendingIterator();
			if (i.hasNext())
			{
				System.out.println("last to land for " + airline + " "
						+ i.next());
			}
		}
	}

	/*
	 * Search method - Search by Flight Number.
	 */
	public Plane searchByFlightNumber(String flightNumber)
	{
		/*
		 * Loop through map.
		 */
		for (String key : map.keySet())
		{
			/*
			 * Place flight number into TreeSet.
			 */
			TreeSet<Plane> set = map.get(key);
			
			/*
			 * Loop through planes.
			 * If flight number exists then return it.
			 */
			for (Plane p : set)
			{
				if (p.getFlightNumber().equalsIgnoreCase(flightNumber))
				{
					return p;
				}
			}
		}
		/*
		 * If it's not there return null.
		 */
		return null;
	}

	/*
	 * Fuel consumption method.
	 * If fuel consumption has that airplane, then return its fuel consumption.
	 * else return 0.
	 */
	public double getFuelConsumption(String airplaneType)
	{
		if (fuelConsumption.containsKey(airplaneType) == true)
		{
			return fuelConsumption.get(airplaneType);
		}
		else
		{
			System.out.println("No plane by that airplaneType");
			return 0;
		}
	}

	/*
	 * Get time limit before land method.
	 * Gets the airplane type.
	 * Calculates fuel remaining by fuel consumption.
	 * Returns calculation for time before land.
	 */
	public double getTimeLimitBeforeLand(String airplaneType,double fuelRemaining)
	{

		double timeLimitBeforeLand;
		double fuelConsumption = getFuelConsumption(airplaneType);
		timeLimitBeforeLand = fuelRemaining / fuelConsumption;
		return timeLimitBeforeLand;
	}

	/*
	 * Copy method - used to calculate time overdue.
	 */
	public void copy()
	{
		/*
		 * ArrayList of planes.
		 * Using Comparator to sort order.
		 */
		ArrayList<Plane> copyList;
		PlaneTimeOverdueComparator overDue = new PlaneTimeOverdueComparator(-1);
		
		/*
		 * Looping through map.
		 */
		for (String key : map.keySet())
		{
			/*
			 * Map of planes is placed into copyList.
			 */
			copyList = new ArrayList<Plane>(this.map.get(key));
			/*
			 * Calling collection.sort
			 */
			Collections.sort(copyList, overDue);
			/*
			 * Printing copyList.
			 */
			for (Plane p : copyList)
			{
				System.out.println(p);
			}
		}
	}

	public void sort(PlaneTimeOverdueComparator comp)
	{
		ArrayList<Plane> copyList;
		PlaneTimeOverdueComparator overDue = new PlaneTimeOverdueComparator(-1);
		/*
		 * Sorting copyList using Comparator.
		 */
		for (String key : map.keySet())
		{
			copyList = new ArrayList<Plane>(this.map.get(key));
			Collections.sort(copyList, overDue);

		}
	}
	/*
	 * Save & load method
	 */
	public void saveAndLoad()
	{
		/*
		 * Saving a file called Flight System Save.
		 * Storing map object.
		 */
		SerializationUtility.store("Flight System Save", map);
		/*
		 * Loading HashMap String, TreeSet.
		 */
		HashMap<String, TreeSet<Plane>> copyMap = (HashMap<String, TreeSet<Plane>>) SerializationUtility.load("Flight System Save");
		/*
		 * Printing map.
		 */
		System.out.println(copyMap);
	}

}
