/*
 * John McDonnell.
 * OOD - CA2.
 * Flight Control System.
 * Version Final.
 * Due Date: 14/12/2014.
 */
package Control;

/*
 * Imports.
 */
import java.util.ArrayList;
import java.util.Scanner;

public class Menu
{
	/*
	 * Instance variable
	 */
	private String name;
	private ArrayList<String> menuList;
	private Scanner kb;

	/*
	 * Constructor.
	 */
	public Menu(String name)
	{
		this.name = name;
		this.menuList = new ArrayList<String>();
		this.kb = new Scanner(System.in);
	}

	/*
	 * Add to menu.
	 */
	public void add(String option)
	{
		if (!this.menuList.contains(option))
		{
			this.menuList.add(option);
		}
	}

	/*
	 * Print menu.
	 */
	public void print()
	{
		for (int i = 0; i < this.menuList.size(); i++)
		{
			System.out.println((i + 1) + ") " + this.menuList.get(i));
		}
	}

	/*
	 * Print choice and check validation.
	 */
	public int printAndGetMainChoice(String strPrompt)
	{
		print();
		int mainChoice = InputValidationUtility.getInt(this.kb, strPrompt, 1,
				11);
		return mainChoice;
	}

	/*
	 * Returns menu size.
	 */
	public int size()
	{
		return this.menuList.size();
	}

	/*
	 * Clears menu.
	 */
	public void clear()
	{
		this.menuList.clear();
	}

}
