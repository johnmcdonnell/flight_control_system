/*
 * John McDonnell.
 * OOD - CA2.
 * Flight Control System.
 * Version Final.
 * Due Date: 14/12/2014.
 */
package Control;

import java.util.Comparator;
import Model.Plane;

/*
 * Comparator sorts plane objects.
 */

public class PlaneTimeOverdueComparator implements Comparator<Plane>
{
	/*
	 * Declaring sort order.
	 */
	private int sortOrder;

	/*
	 * Sorting object order.
	 */
	public PlaneTimeOverdueComparator(int sortOrder)
	{
		this.sortOrder = sortOrder; // 1 or -1
	}

	/*
	 * Comparing time overdue. Sorting and returning new order.
	 */
	public int compare(Plane p0, Plane p1)
	{
		double difference = p0.getTimeOverdue() - p1.getTimeOverdue();

		if (difference > 0)
		{
			return 1 * sortOrder;
		}
		else if (difference < 0)
		{
			return -1 * sortOrder;
		}
		else
		{
			return 0;
		}
	}

}
